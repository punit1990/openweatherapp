//
//  Double+Extensions.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/8/23.
//

import Foundation

extension Double {
	
	enum TemperatureUnit {
		case celsius
		case fahrenheit
	}
	var convertingKelvinTempToF: Double {
		let fahrenheitTemp = ((self - 273.15) * (9/5)) + 32.0
		return fahrenheitTemp
	}
	
	func formatTemp(for unit: TemperatureUnit) -> String {
		let unitString = (unit == .fahrenheit) ? "f" : "C"
		return String(format: "%.0f° " + unitString, self)
	}
	
	var toString: String {
		"\(self)"
	}
}
