//
//  OpenWeatherNetworkManager.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import Foundation
import Combine

enum OpenWeatherErrors: Error {
    case invalidURL
    case insufficientParams
	case unexpectedType
}

protocol OpenWeatherNetworkManagerProtocol {
    func makeRequest(with param: OpenWeatherAPIParams) async throws -> Decodable?
}

actor OpenWeatherNetworkManager: OpenWeatherNetworkManagerProtocol {
	
    private lazy var session: URLSession = {
        URLSession(configuration: .default)
    }()
    
    var isLoadingPublisher = CurrentValueSubject<Bool, Never>(false)
    
    func makeRequest(with param: OpenWeatherAPIParams) async throws -> Decodable? {		
        guard var urlComponents = URLComponents(string: param.urlString) else {
            throw OpenWeatherErrors.invalidURL
        }
        
        urlComponents.queryItems = param.queryItems
        
        guard let url = urlComponents.url else {
            throw OpenWeatherErrors.invalidURL
        }
		
		do {
			let (data, _) = try await session.data(from: url)
			let returnData = try JSONDecoder().decode(param.returnType, from: data)
			return returnData
		} catch {
			/// Given more time, there would be ore elaborate error handling here
			print("Error in serialization: \(error)")
			return nil
		}
    }
}
