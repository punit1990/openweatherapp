//
//  ImageLoader.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/8/23.
//

import Foundation
import UIKit
import Combine

struct WeatherIconLoader {
    
    static let shared = WeatherIconLoader()
    private let cache = WeatherImageCache()
    private let cancellables = Set<AnyCancellable>()
    private let iconURLBaseString = "https://openweathermap.org/img/wn/"
    
    func loadImage(for icon: String) -> AnyPublisher<UIImage?, Never>? {
        // https://openweathermap.org/img/wn/10d@2x.png
        // Icons at 3x dont seem to be working, hardcoding 2x for now
        guard let url = URL(string: iconURLBaseString + icon + "@2x.png") else { return nil }
        if let image = cache.image(for: url) {
            return Just(image).eraseToAnyPublisher()
		}
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { (data, _) -> UIImage? in
				let image = UIImage(data: data)
				cache.insert(image, for: url)
				return image
			}
            .catch { error in return Just(nil) }
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
