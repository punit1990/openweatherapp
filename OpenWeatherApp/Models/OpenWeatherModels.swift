//
//  OpenWeatherModels.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import Foundation
import Combine
import UIKit

protocol LocationProvider {
	var latitude: String { get }
	var longitude: String { get }
}

/// Protocol for representing the data (model layer) for each cell
protocol OpenWeatherCellDataModel {
	var location: Location { get }
	func toCellViewModel() -> OpenWeatherCellViewModelProtocol
}

/// Represents a search result output, provides lat lon for GET weather call upon selection
struct LocationSearchResult: Decodable, OpenWeatherCellDataModel {
    
    enum CodingKeys: String, CodingKey {
        case name
        case latitude = "lat"
        case longitude = "lon"
        case country
        case state
    }
    
    let name: String
    let latitude: Double
    let longitude: Double
    let country: String
    let state: String?
	var location: Location { Location(latitude: latitude.toString, longitude: longitude.toString) }
	
    func toCellViewModel() -> OpenWeatherCellViewModelProtocol {
		SearchLocationCellViewModel(with: self, locationId: location.locationId)
    }
}

/// Represents weather info for a city
struct WeatherData: Decodable, OpenWeatherCellDataModel {
    let base: String
    let clouds: Cloud
    let cod: Int
    let coord: CoordinatesData
    let id: Int
    let main: Main
    let name: String
    /// TODO: Even though this is an array, we only consider first element in the array for UI,
    /// given more time, I'd like to explore more into scenarios where there are more than 1
    /// elements here
    let weather: [Weather]
    let wind: Wind
	var location: Location {
		Location(latitude: coord.latitude.toString, longitude: coord.longitude.toString)
	}

    func toCellViewModel() -> OpenWeatherCellViewModelProtocol {
		WeatherCellViewModel(with: self, location: location)
    }
}

struct Location: Codable, Equatable {
	let latitude: String
	let longitude: String
	var locationId: String {
		latitude + "," + longitude
	}
}

struct Wind: Decodable {
    let deg: Int
    let gust: Double?
    let speed: Double
}

struct Cloud: Decodable {
    let all: Int
}

struct Main: Decodable {
    enum CodingKeys: String, CodingKey {
        case feelsLike = "feels_like"
        case humidity
        case pressure
        case temp
        case maxTemp = "temp_max"
        case minTemp = "temp_min"
    }
    
    let feelsLike: Double
    let humidity: Int
    let pressure: Int
    let temp: Double
    let maxTemp: Double
    let minTemp: Double
}

struct CoordinatesData: Decodable {
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
    }
    
    let latitude: Double
    let longitude: Double
}

struct Weather: Decodable {
    let description: String
    let icon: String
    let id: Int
    let main: String
}
