//
//  OpenWeatherRequestParams.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import Foundation

protocol OpenWeatherAPIParams {
    var urlString: String { get }
    var queryItems: [URLQueryItem] { get }
    var returnType: Decodable.Type { get }
}

enum SearchWeatherParam: OpenWeatherAPIParams {
    case byGeoLoc(latitude: String, longitude: String)
    case byCity(cityInfo: String)
    
    var queryItems: [URLQueryItem] {
        var queryItems = [
            URLQueryItem(name: "APPID", value: "ceeef3406441b38a46a16922a8076c80"),
            URLQueryItem(name: "limit", value: "100")
        ]
        switch self {
        case .byGeoLoc(let latitude, let longitude):
            queryItems += [
                URLQueryItem(name: "lat", value: "\(latitude)"),
                URLQueryItem(name: "lon", value: "\(longitude)")
            ]
        case .byCity(let cityInfo):
            queryItems += [
                URLQueryItem(name: "q", value: cityInfo)
            ]
        }
        
        return queryItems
    }
    
    var urlString: String { "https://api.openweathermap.org/data/2.5/weather" }
    var returnType: Decodable.Type { WeatherData.self }

}

enum SearchLocationParam: OpenWeatherAPIParams {
    case location(queryItem: String)
    
    var queryItems: [URLQueryItem] {
        var queryItems = [
            URLQueryItem(name: "appid", value: "ceeef3406441b38a46a16922a8076c80"),
            URLQueryItem(name: "limit", value: "100")
        ]
        switch self {
            case .location(let queryItem):
                queryItems += [URLQueryItem(name: "q", value: queryItem)]
        }
        return queryItems
    }
    
    var urlString: String { "https://api.openweathermap.org/geo/1.0/direct" }
    var returnType: Decodable.Type { [LocationSearchResult].self }
}
