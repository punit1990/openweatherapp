//
//  SavedLocationsHandler.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import Foundation
import UIKit

struct SavedLocationsHandler {
    private let savedLocationsKey = "savedLocations"
	private let userDefaults: UserDefaults
	static let shared = SavedLocationsHandler()

	private init(with userDefaults: UserDefaults = UserDefaults.standard) {
		self.userDefaults = userDefaults
	}
    
    var locations: [String: Location] {
        if let data = UserDefaults.standard.object(forKey: savedLocationsKey) as? Data,
           let category = try? JSONDecoder().decode([String: Location].self, from: data) {
             return category
        }
        return [:]
    }
    
    func saveLocation(location: Location) {
        var savedLocations = locations
        savedLocations[location.locationId] = location
		print("Saved Location: \(location.locationId)")
		

        if let encoded = try? JSONEncoder().encode(savedLocations) {
            userDefaults.set(encoded, forKey: savedLocationsKey)
        }
    }
	
	func removeLocation(for locationId: String) {
		var savedLocations = locations
		savedLocations[locationId] = nil

		if let encoded = try? JSONEncoder().encode(savedLocations) {
			userDefaults.set(encoded, forKey: savedLocationsKey)
		}
		
		print("Removed Location: \(locationId)")
	}
}
