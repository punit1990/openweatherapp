//
//  WeatherImageCache.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/8/23.
//

import Foundation
import UIKit

final class WeatherImageCache {
	
	static let shared = WeatherImageCache()
	
	private lazy var imageCache: NSCache<AnyObject, AnyObject> = {
		let cache = NSCache<AnyObject, AnyObject>()
		return cache
	}()

	private let lock = NSLock()
	
	func insert(_ image: UIImage?, for url: URL) {
		guard let image else { return removeImage(for: url) }

		lock.lock()
		defer { lock.unlock() }
		imageCache.setObject(image, forKey: url as AnyObject)
	}

	func removeImage(for url: URL) {
		lock.lock()
		defer { lock.unlock() }
		imageCache.removeObject(forKey: url as AnyObject)
	}
	
	func image(for url: URL) -> UIImage? {
		lock.lock()
		defer { lock.unlock() }

		if let image = imageCache.object(forKey: url as AnyObject) as? UIImage {
			return image
		}
		return nil
	}
}
