//
//  OpenWeatherCellViewModels.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/8/23.
//

import Foundation

/// OpenWeatherCellViewModelProtocol allows to generate common interface for Location and Weather cells.
/// This allows ViewController to remain unaware of which cell is actually being rendered
protocol OpenWeatherCellViewModelProtocol {
	var cellIdentifier: String { get }
	var cellType: OpenWeatherCellProtocol.Type { get }
	var location: Location { get }
}

struct SearchLocationCellViewModel: OpenWeatherCellViewModelProtocol {
	let text: String
	let location: Location

	/// OpenWeatherCellViewModelProtocol implemenation
	let cellIdentifier = "searchLocationCell"
	let cellType: OpenWeatherCellProtocol.Type = SearchLocationCell.self

	/// Construct City, State, Country string from LocationSearchResult
	init(with searchResult: LocationSearchResult, locationId: String) {
		let nameString = searchResult.name
		var stateString = ""
		if let state = searchResult.state {
			stateString = ", \(state)"
		}
		let countryString = ", \(searchResult.country)"
		self.text = nameString + stateString + countryString
		self.location = Location(latitude: searchResult.latitude.toString, longitude: searchResult.longitude.toString)
	}
}


struct WeatherCellViewModel: OpenWeatherCellViewModelProtocol {
	let location: Location
	let cityName: String
	var isLoadingImage: Bool = false
	var imageData: Data?
	let weatherDescription: String
	let temperature: String
	let feelsLike: String
	let minTemp: String
	let maxTemp: String
	let windSpeed: String
	let icon: String?
	
	/// OpenWeatherCellViewModelProtocol implemenation
	let cellIdentifier = "weatherCell"
	let cellType: OpenWeatherCellProtocol.Type = WeatherCell.self

	init(with weatherData: WeatherData, location: Location) {
		self.cityName = weatherData.name
		self.weatherDescription = weatherData.weather.first?.description ?? ""
		self.temperature = weatherData.main.temp.convertingKelvinTempToF.formatTemp(for: .fahrenheit)
		self.feelsLike = "Feels like: " + weatherData.main.feelsLike.convertingKelvinTempToF.formatTemp(for: .fahrenheit)
		self.minTemp = "Min: " + weatherData.main.minTemp.convertingKelvinTempToF.formatTemp(for: .fahrenheit)
		self.maxTemp = "Max: " + weatherData.main.maxTemp.convertingKelvinTempToF.formatTemp(for: .fahrenheit)
		self.windSpeed = "Wind speed: " + String(format: "%.0f", weatherData.wind.speed) + " mph"
		self.icon = weatherData.weather.first?.icon
		self.location = location
	}
}
