//
//  SearchLocationCell.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import UIKit
import Combine


protocol OpenWeatherCellProtocol: UITableViewCell {
    func setup(with cellViewModel: OpenWeatherCellViewModelProtocol)
}

class SearchLocationCell: UITableViewCell, OpenWeatherCellProtocol {
    func setup(with cellViewModel: OpenWeatherCellViewModelProtocol) {
        guard let searchLocationCellViewModel = cellViewModel as? SearchLocationCellViewModel else {
            // TODO: Given more time there could be more elaborate exception handling implemented here
            return
        }
        self.textLabel?.text = searchLocationCellViewModel.text
    }
}

class WeatherCell: UITableViewCell, OpenWeatherCellProtocol {
    
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var imageActivityIndicator: UIActivityIndicatorView!
    private var cancellables = Set<AnyCancellable>()
    
    func setup(with cellViewModel: OpenWeatherCellViewModelProtocol) {
        guard let weatherCellViewModel = cellViewModel as? WeatherCellViewModel else {
            // TODO: Given more time there could be more elaborate exception handling implemented here
            return
        }
        city.text = weatherCellViewModel.cityName
        weatherDescription.text = weatherCellViewModel.weatherDescription
        temperature.text = weatherCellViewModel.temperature
        feelsLike.text = weatherCellViewModel.feelsLike
        minTemp.text = weatherCellViewModel.minTemp
        maxTemp.text = weatherCellViewModel.maxTemp
        windSpeed.text = weatherCellViewModel.windSpeed
        imageActivityIndicator.startAnimating()
        imageActivityIndicator.hidesWhenStopped = true
        
		if
			let icon = weatherCellViewModel.icon,
			let iconPublisher = WeatherIconLoader.shared.loadImage(for: icon) {
            iconPublisher
                .receive(on: DispatchQueue.main)
                .sink { [weak self] image in
                    guard let self else { return }
                    self.weatherImageView.image = image
                    self.weatherImageView.isHidden = false
                    self.imageActivityIndicator.stopAnimating()
                }.store(in: &cancellables)
        }
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        city.text = nil
        weatherDescription.text = nil
        temperature.text = nil
        feelsLike.text = nil
        minTemp.text = nil
        maxTemp.text = nil
        windSpeed.text = nil
        imageActivityIndicator.isHidden = false
        imageActivityIndicator.startAnimating()
		weatherImageView.isHidden = true
		weatherImageView.image = nil
        cancellables.removeAll()
    }
}
