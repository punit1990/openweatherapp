//
//  SearchLocationViewController.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import UIKit
import Combine
import CoreLocation

class OpenWeatherViewController: UIViewController {

	@IBOutlet private weak var editButton: UIBarButtonItem!
	@IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    private var timer: Timer?
    private var cancellables = Set<AnyCancellable>()
    private var refreshControl = UIRefreshControl()
	private let locationManager = CLLocationManager()

    private lazy var viewModel: OpenWeatherViewModelProtocol = {
        OpenWeatherViewModel()
    }()
	var shouldHideEdit: Bool {
		/// Only weather cells are editable
		viewModel.dataPublisher.value.filter { $0 is WeatherCellViewModel }.isEmpty
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
		locationManager.delegate = self
		
        tableView.refreshControl = refreshControl
		refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)

		Task {
			// This is being sent on Mainactor, no need to change thread here
			for await _ in viewModel.dataPublisher.values {
				if #available(iOS 16, *) {
					editButton.isHidden = self.shouldHideEdit
				}
				tableView.reloadData()
			}
		}
        
		Task {
			// This is being sent on Mainactor, no need to change thread here
			for await isLoading in viewModel.isLoadingPublisher.values {
				if isLoading {
					refreshControl.beginRefreshing()
				} else {
					refreshControl.endRefreshing()
				}
			}
		}
		
		// Populate some testing data in case the table is empty
		prepopulateTableIfNeeded()
		
		Task {
			await viewModel.fetchWeatherForSavedLocations()
		}
    }
	
	
	/// Prepopulate table with some data for testing
	private func prepopulateTableIfNeeded() {
		/// If data was already pre-populated, dont populate it again
		guard SavedLocationsHandler.shared.locations.values.count == 0 else {
			return
		}
		[
			Location(latitude: "40.7127281", longitude: "-74.0060152"),
			Location(latitude: "52.5170365", longitude: "13.3888599"),
			Location(latitude: "19.2396742", longitude: "73.1366482"),
			Location(latitude: "33.5914237", longitude: "73.0535122"),
			Location(latitude: "33.7489924", longitude: "-84.3902644"),
			Location(latitude: "48.4283182", longitude: "-123.3649533"),
			Location(latitude: "41.8755616", longitude: "-87.6244212"),
			Location(latitude: "37.3361663", longitude: "-121.890591"),
			Location(latitude: "43.5460516", longitude: "-80.2493276"),
			Location(latitude: "43.157285", longitude: "-77.615214"),
			Location(latitude: "51.0460954", longitude: "-114.065465"),
			Location(latitude: "34.0007493", longitude: "-81.0343313")
		].forEach { SavedLocationsHandler.shared.saveLocation(location: $0) }
	}

	@IBAction func editTapped(_ sender: Any) {
		tableView.setEditing(!tableView.isEditing, animated: true)
		editButton.title = tableView.isEditing ? "Done" : "Edit"
	}
	
	@IBAction func fetchLocation(_ sender: Any) {
		switch locationManager.authorizationStatus {
			case .notDetermined:
				locationManager.requestWhenInUseAuthorization()
			case .restricted, .denied:
				showPermissionDeniedAlert()
			case .authorizedAlways, .authorizedWhenInUse:
				locationManager.requestLocation()
			@unknown default:
				break
		}
	}
	
	private func showPermissionDeniedAlert() {
		/// Given more time, I can set up the OK button to go to app settings and enable location there
		let alertController = UIAlertController(title: "Permission Denied", message: "Please enable location permission for this app in the app settings", preferredStyle: .alert)
		present(alertController, animated: true)
	}
	
	@objc func refresh(_ sender: AnyObject) {
		Task {
			await viewModel.fetchWeatherForSavedLocations()
		}
	}
}

extension OpenWeatherViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.dataPublisher.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let isCellViewModelAvailableForIndexpath = indexPath.row < viewModel.dataPublisher.value.count
        guard isCellViewModelAvailableForIndexpath else { return UITableViewCell() }
        let cellViewModel = viewModel.dataPublisher.value[indexPath.row]
        
        guard let tableRowCell = tableView.dequeueReusableCell(withIdentifier: cellViewModel.cellIdentifier, for: indexPath) as? OpenWeatherCellProtocol else {
            return UITableViewCell()
        }
        tableRowCell.setup(with: cellViewModel)
        return tableRowCell
    }
}

extension OpenWeatherViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let locationViewModel = viewModel.dataPublisher.value[indexPath.row] as? SearchLocationCellViewModel {
            let location = locationViewModel.location
			Task {
				await viewModel.fetchWeather(for: location)
			}
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		let isCellViewModelAvailableForIndexpath = indexPath.row < viewModel.dataPublisher.value.count
		guard isCellViewModelAvailableForIndexpath else { return false }
		let cellViewModel = viewModel.dataPublisher.value[indexPath.row]
		return (cellViewModel is WeatherCellViewModel)
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete,
		   indexPath.row < viewModel.dataPublisher.value.count {
			let cellViewModel = viewModel.dataPublisher.value[indexPath.row]
			viewModel.deleteSavedLocation(for: cellViewModel)
			tableView.deleteRows(at: [indexPath], with: .automatic)
		}
	}
}

extension OpenWeatherViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { [weak self] _ in
            guard let self else { return }
			
			Task {
				if !searchText.isEmpty {
					await self.viewModel.fetchLocations(for: searchText)
				}
			}
        })
    }
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		if tableView.isEditing {
			tableView.setEditing(false, animated: true)
		}
	}

}

extension OpenWeatherViewController: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let location = locations.first {
			let location = Location(latitude: location.coordinate.latitude.toString, longitude: location.coordinate.longitude.toString)
			Task {
				await viewModel.fetchWeather(for: location)
			}
		}
	}

	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		/// Fail silently
	}
	
	func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
		switch locationManager.authorizationStatus {
		case .restricted, .denied:
			/// Do nothing, perhps add an alert saying location has been denied
			break
		default:
			locationManager.requestLocation()
		}
	}
}
