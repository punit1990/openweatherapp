//
//  SearchLocationViewModel.swift
//  OpenWeatherApp
//
//  Created by Punit Kulkarni on 5/7/23.
//

import Foundation
import Combine
import UIKit

protocol OpenWeatherViewModelProtocol {
    @MainActor var dataPublisher: CurrentValueSubject<[OpenWeatherCellViewModelProtocol], Never> { get }
	@MainActor var isLoadingPublisher: CurrentValueSubject<Bool, Never> { get }
    
	func deleteSavedLocation(for viewModel: OpenWeatherCellViewModelProtocol)
	
	/// Data load methods
    func fetchLocations(for searchText: String) async
    func fetchWeatherForSavedLocations() async
    func fetchWeather(for location: Location) async
}

class OpenWeatherViewModel: OpenWeatherViewModelProtocol {
    
	@MainActor let dataPublisher = CurrentValueSubject<[OpenWeatherCellViewModelProtocol], Never>([])
	@MainActor let isLoadingPublisher = CurrentValueSubject<Bool, Never>(false)

    let dataManager: OpenWeatherNetworkManagerProtocol = OpenWeatherNetworkManager()
	private let searchResultsPublisher = CurrentValueSubject<[OpenWeatherCellViewModelProtocol], Never>([])
	private let savedLocationsPublisher = CurrentValueSubject<[OpenWeatherCellViewModelProtocol], Never>([])
	
	private var savedLocations: [String: Location] { SavedLocationsHandler.shared.locations }
    private var cancellables = Set<AnyCancellable>()

    init() {
		// There has to be a better way for showing the most recent output among savedLocationsPublisher
		// and searchResultsPublisher on dataPublisher, given more time, I'd like to look into alternate solutions
		savedLocationsPublisher
			.sink { [weak self] locations in
				guard let self else { return }
				self.dataPublisher.send(locations)
			}.store(in: &cancellables)
		
		searchResultsPublisher
			.sink { [weak self] searchResults in
				guard let self else { return }
				self.dataPublisher.send(searchResults)
			}.store(in: &cancellables)
    }
	
	func deleteSavedLocation(for viewModel: OpenWeatherCellViewModelProtocol) {
		SavedLocationsHandler.shared.removeLocation(for: viewModel.location.locationId)
		savedLocationsPublisher.value.removeAll { $0.location.locationId == viewModel.location.locationId }
	}

    func fetchLocations(for searchText: String) async {
		isLoadingPublisher.send(true)
        do {
			let searchResultsDecodable = try await dataManager.makeRequest(with: SearchLocationParam.location(queryItem: searchText))
			guard
				let searchResults = (searchResultsDecodable as? [OpenWeatherCellDataModel]) else {
				self.dataPublisher.send([])
				return
			}
			
			let cellModels = searchResults.map { $0.toCellViewModel() }
			searchResultsPublisher.send(cellModels)
        } catch {
            searchResultsPublisher.send([])
        }
		isLoadingPublisher.send(false)
    }
    
	/// Right now, we load all saved cells together. Ideally, given more time, would like to look into
	/// loading each cell paralllely displaying a template the city name and spinner for the placeholder cell
	/// during the weather data fetch
	///
	///  This function loads the weather for all saved cities asynchronously
    func fetchWeatherForSavedLocations() async {
		isLoadingPublisher.send(true)
		
		let cellModels = await withTaskGroup(of: OpenWeatherCellViewModelProtocol?.self, returning: [OpenWeatherCellViewModelProtocol].self) { taskGroup in
			for location in self.savedLocations.values  {
				taskGroup.addTask {
					let savedLocation = try? await self.dataManager.makeRequest(with: SearchWeatherParam.byGeoLoc(latitude: location.latitude, longitude: location.longitude)) as? OpenWeatherCellDataModel
					return savedLocation?.toCellViewModel()
				}
			}

			var cellModels: [OpenWeatherCellViewModelProtocol] = []
			for await result in taskGroup {
				if let cellModel = result {
					cellModels.append(cellModel)
				}
			}
			return cellModels
		}
		
		savedLocationsPublisher.send(cellModels)
		isLoadingPublisher.send(false)
    }
    
    func fetchWeather(for location: Location) async {
		isLoadingPublisher.send(true)
			
		guard let savedLocation = await fetchSingleWeather(for: location) else {
			return
		}
		
		let cellModel = savedLocation.toCellViewModel()
		let location = savedLocation.location
		
		/// Save the location
		if savedLocations[location.locationId] == nil {
			SavedLocationsHandler.shared.saveLocation(location: location)
		}
		
		let filteredLocations = savedLocationsPublisher.value.filter { $0.location.locationId != cellModel.location.locationId }
		savedLocationsPublisher.send([cellModel] + filteredLocations)
		
		isLoadingPublisher.send(false)
    }
	
	private func fetchSingleWeather(for location: Location) async -> OpenWeatherCellDataModel? {
		do {
			let savedLocationDecodable = try await dataManager.makeRequest(with: SearchWeatherParam.byGeoLoc(latitude: location.latitude, longitude: location.longitude))
			guard
				let savedLocation = savedLocationDecodable as? OpenWeatherCellDataModel else {
				return nil
			}

			return savedLocation
		} catch {
			/// Given more time, would like to add more elaborate exception handling here
		}
		
		return nil
	}
}
