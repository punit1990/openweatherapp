//
//  SavedLocationsHandlerTests.swift
//  OpenWeatherAppTests
//
//  Created by Punit Kulkarni on 5/8/23.
//

import XCTest
@testable import OpenWeatherApp

final class SavedLocationsHandlerTests: XCTestCase {

	let handler = SavedLocationsHandler.shared
	var location: Location?
	
	func testSaveLocation() throws {
		try givenLocation()
		try whenSaveLocation()
		try thenValidateStoredLocation()
	}

	func testRemove() throws {
		try givenLocation()
		try whenSaveLocation()
		try whenRemovedImageInCache()
		try thenValidateRemovedLocation()
	}
	
	// MARK: Given
	func givenLocation() throws {
		location = Location(latitude: "1234.0", longitude: "1234.0")

	}
	
	// MARK: When
	func whenSaveLocation() throws {
		let location = try XCTUnwrap(location)
		handler.saveLocation(location: location)
	}
	
	func whenRemovedImageInCache() throws {
		let location = try XCTUnwrap(location)
		handler.removeLocation(for: location.locationId)
	}
	
	// MARK: Then
	func thenValidateStoredLocation() throws {
		let location = try XCTUnwrap(location)
		let storedLocation = handler.locations[location.locationId]
		XCTAssertEqual(storedLocation, location)
	}
	
	func thenValidateRemovedLocation() throws {
		let location = try XCTUnwrap(location)
		XCTAssertNil(handler.locations[location.locationId])
	}
}
