//
//  WeatherImageCacheTests.swift
//  OpenWeatherAppTests
//
//  Created by Punit Kulkarni on 5/8/23.
//

import XCTest
@testable import OpenWeatherApp

final class WeatherImageCacheTests: XCTestCase {

	let imageCache = WeatherImageCache.shared
	var url: URL?
	var image: UIImage?
	
	override func tearDown() {
		super.tearDown()
		url = nil
		image = nil
	}
	
	func testInsert() throws {
		try givenURLAndImage()
		try whenInsertedInCache()
		try thenValidateStoredImage()
	}

	func testRemove() throws {
		try givenURLAndImage()
		try whenInsertedInCache()
		try whenRemovedImageInCache()
		try thenValidateRemovedImage()
	}
	
	// MARK: Given
	func givenURLAndImage() throws {
		url = try XCTUnwrap(URL(string: "www.ebay.com"))
		image = UIImage(color: .yellow, size: CGSize(width: 100, height: 100))
	}
	
	// MARK: When
	func whenInsertedInCache() throws {
		let image = try XCTUnwrap(image)
		let url = try XCTUnwrap(url)
		imageCache.insert(image, for: url)
	}
	
	func whenRemovedImageInCache() throws {
		let url = try XCTUnwrap(url)
		imageCache.removeImage(for: url)
	}
	
	// MARK: Then
	func thenValidateStoredImage() throws {
		let url = try XCTUnwrap(url)
		let storedImage = imageCache.image(for: url)
		XCTAssertEqual(storedImage, image)
	}
	
	func thenValidateRemovedImage() throws {
		let url = try XCTUnwrap(url)
		XCTAssertNil(imageCache.image(for: url))
	}
}

private extension UIImage {
	  convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
		let rect = CGRect(origin: .zero, size: size)
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		color.setFill()
		UIRectFill(rect)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		guard let cgImage = image?.cgImage else { return nil }
		self.init(cgImage: cgImage)
	  }
	}
