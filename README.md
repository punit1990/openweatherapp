# OpenWeatherApp



## Project Structure

- Used MVVM to construct a single viewController layout
- ![Alt text](OpenWeatherApp/flowchart.png?raw=true "Overview")
- ![Alt text](OpenWeatherApp/demo.mp4 "Demo")

## Future advancements

- Given more time I'd like to change the datastructure for the display of elements from rictionary to array so I can keep the 
  location of the elements in the table fixed and show a placeholder during weather fetch. 
- This would also allow each weather to be load individually in the cell rather than having to wait for the slowest call
  to load all weathers
